export function initHighScore() {
    const highScoresList = document.querySelector('#highScoresList')
    const highScores = JSON.parse(localStorage.getItem('highScores')) || [];
    highScoresList.innerHTML =
        highScores.map(score => {
            return `<li class = "high-score">${escapeHtml(score.name)} - ${score.score}</li>`
        }).join('');

// Jako htmlspecialchars v php ale pro JS
    function escapeHtml(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }
}
