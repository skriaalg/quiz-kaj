import startPage from "../JS/Views/startPage.js";
import highscorePage from "../JS/Views/highscorePage.js";
import gamePage from "../JS/Views/gamePage.js";
import endPage from "../JS/Views/endPage.js";
import {initHighScore} from "./highScores.js";
import {initEnd} from "./end.js";
import {initGame} from "./game.js";
import {timerId} from "./game.js";
import {clearTimer} from "./game.js";
import {audio} from "./game.js";

//vytvari jiny obsah podle api
export const navigateTo = url => {
    history.pushState(null, null, url);
    //location.hash = url
    router();
};

//smerovac hleda ty url api
const router = async () => {
    const routes = [
        {path: "#start", view: startPage},
        {path: "#highscores", view: highscorePage},
        {path: "#game", view: gamePage},
        {path: "#end", view: endPage}
    ];

    // Test each route for potential match
    //hledam v jakem jsem api
    const potentialMatches = routes.map(route => {
        //console.log(location.hash)
        return {
            route: route,
            //Tady uz jen konrtoluju hash URL
            isMatch: location.hash === route.path
        };
    });

    //hledam to url ve kterem jsem
    let match = potentialMatches.find(potentialMatch => potentialMatch.isMatch);

    //pokud nenajdu tak default start
    if (!match) {
        match = {
            route: routes[0],
            isMatch: true
        };
    }
    //create instance of page
    const view = new match.route.view();
    //draw html - jakoby prekreslovani stranky index.html
    document.querySelector(".container").innerHTML = await view.getHtml();

    //vykreslovani obsahu podle url cesty a pouziti js codu podle tech cest
    switch (match.route.path) {
        case routes[0].path:
            clerLocalTimer();
            stopAudio();
            break;
        case routes[1].path:
            clerLocalTimer();
            stopAudio();
            initHighScore();
            break;
        case routes[2].path:
            clerLocalTimer();
            initGame();
            break;
        case routes[3].path:
            clerLocalTimer();
            initEnd();
            break;
    }
};

//pro historii back tlacitka
window.addEventListener("popstate", router);

//pokud stisknu na element ktery ma attribute data-link
document.body.addEventListener("click", e => {
    if (e.target.matches("[data-link]")) {
        e.preventDefault();
        //console.log(e.target.dataset['ref'])
        navigateTo(e.target.dataset['ref']);
    }
});

function clerLocalTimer() {
    if (timerId != null) {
        clearTimeout(timerId);
        clearTimer();
        console.log("Timer is clear")
    }
}

function stopAudio() {
    audio.pause();
    audio.currentTime = 0;
}

router();
