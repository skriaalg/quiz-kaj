import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor() {
        super();
        this.setTitle("end page");
    }

    async getHtml() {
        return `               
                <section id="end" class="flex-center flex-column">
                    <h1 id="finalScore">0</h1>
                    <form class="end-form-container">
                        <h2 id="end-text">Enter your name below to save your score</h2>
                        <input type="text" name="name" id="username" placeholder="Enter your name">
                        <button class="btn" id="saveScoreBtn" type="submit" disabled>Save</button>
                    </form>
                    <a data-ref="#game" class="btn" data-link>Play again</a>
                    <a data-ref="#start" class="btn" data-link>Go home <i class="fas fa-home"></i></a>
                </section>
        `;
    }
}