import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor() {
        super();
        this.setTitle("High score page");
    }

    async getHtml() {
        return `
                    <section id="highScores" class="flex-center flex-column">
                        <h1 id="finalScore">Leaderboard</h1>
                        <ul id="highScoresList"></ul>
                        <a data-ref="#start" class="btn" data-link>Go home <i class="fas fa-home"></i></a>
                    </section>
            `;
    }
}