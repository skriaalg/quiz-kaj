import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor() {
        super();
        this.setTitle("game page");
    }

    async getHtml() {
        return ` 
                <main id="game" class="justify-center flex-column">
                    <section id="hud">
                        <div class="hud-item">
                            <p id="progressText" class="hud-prefix">
                                Question
                            </p>
                            <div id="progressBar">
                                <div id="progressBarFull">
                                </div>
                            </div>
                        </div>
                        <div class="hud-item">
                            <p class="hud-prefix">
                                Score
                            </p>
                            <h1 class="hud-main-text" id="score">
                                0
                            </h1>
                        </div>
                    </section>
                    <section>
                        <h1 id="question">What is the answer to this question</h1>
                        <ul>
                            <li class="choice-container">
                                <p class="choice-prefix">A</p>
                                <p class="choice-text" data-number="1">Choice</p>
                            </li>
                            <li class="choice-container">
                                <p class="choice-prefix">B</p>
                                <p class="choice-text" data-number="2">Choice2</p>
                            </li>
                            <li class="choice-container">
                                <p class="choice-prefix">C</p>
                                <p class="choice-text" data-number="3">Choice3</p>
                            </li>
                            <li class="choice-container">
                                <p class="choice-prefix">D</p>
                                <p class="choice-text" data-number="4">Choice4</p>
                            </li>
                        </ul>     
                    </section>
                </main>
        `;
    }
}