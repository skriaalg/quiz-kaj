import AbstractView from "./AbstractView.js";

export default class extends AbstractView {
    constructor() {
        super();
        this.setTitle("Start page");
    }

    async getHtml() {
        return `
                    <section id="home" class="flex-column flex-center">
                        <h1>Are you ready?</h1>
                        <a data-ref="#game" class="btn" data-link>Play Quiz</a>
                        <a data-ref="#highscores" id="highscore-btn"
                           class="btn" data-link>High scores <i class="fas fa-crown"></i></a>
                    </section> 
        `;
    }
}