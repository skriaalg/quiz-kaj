import {navigateTo} from "./index.js";

export var timerId = null;
export var audio = new Audio('../Audio/chill.mp3');

export function clearTimer() {
    timerId = null;
}

export function initGame() {
    let questions = [
        {
            question: 'What is 2+2',
            choice1: '2',
            choice2: '3',
            choice3: '4',
            choice4: '5',
            answer: '3'
        },
        {
            question: 'What is 5*5',
            choice1: '24',
            choice2: '35',
            choice3: '20',
            choice4: '25',
            answer: '4'
        },
        {
            question: 'Proces ověření oprávnění přístupu k systémovým zdrojům lze označit slovem: ',
            choice1: 'zabezpečení',
            choice2: 'autentizace',
            choice3: 'důvěryhodnost',
            choice4: 'autorizace ',
            answer: '4'
        },
        {
            question: 'Která operace v rámci jedné rundy je z hlediska bezpečnosti DESu nejdůležitější? ',
            choice1: 'substituce S-boxu',
            choice2: 'přičítání klíčů v K-boxu ',
            choice3: 'prohazování polovin bloků mezi rundami',
            choice4: 'permutace P-boxu',
            answer: '1'
        }
    ]

    const SCORE_POINTS = 100;
    const MAX_QUESTIONS = 4;

    class StartGame {
        constructor() {
            this.question = document.querySelector('#question');
            this.choices = Array.from(document.querySelectorAll('.choice-text'));
            this.progressText = document.querySelector('#progressText');
            this.scoreText = document.querySelector('#score');
            this.progressBarFull = document.querySelector('#progressBarFull');

            this.currentQuestion = {};
            this.acceptingAnswers = true;
            this.score = 0;
            this.questionCounter = 0;
            this.availableQuestions = [];
            this.start();
        }

        playAudio() {
            audio.loop = true;
            audio.volume = 0.5;
            audio.play();
        }

        start() {
            this.questionCounter = 0
            this.score = 0
            this.availableQuestions = [...questions]
            this.newquest();
            this.playAudio();
        }

        newquest() {
            if (this.availableQuestions.length === 0 || this.questionCounter > MAX_QUESTIONS) {
                //set last score
                localStorage.setItem('mostRecentScore', this.score);
                navigateTo("#end");
                return;
            }

            this.questionCounter++;
            //progress bar title
            this.progressText.innerText = `Question ${this.questionCounter} of ${MAX_QUESTIONS}`;
            //style bar
            this.progressBarFull.style.width = `${(this.questionCounter / MAX_QUESTIONS) * 100}%`;
            //random question
            const questionsIndex = Math.floor(Math.random() * this.availableQuestions.length)
            this.currentQuestion = this.availableQuestions[questionsIndex];
            //write question to html
            this.question.innerText = this.currentQuestion.question;
            //write choice to html, dataset = data-number attribute
            this.choices.forEach(choice => {
                const number = choice.dataset['number']
                choice.innerText = this.currentQuestion['choice' + number]
            })
            //delete question from availableQuestions
            this.availableQuestions.splice(questionsIndex, 1)
            this.acceptingAnswers = true;

            this.createChoices();
        }

        createChoices() {
            this.choices.forEach(choice => {
                choice.addEventListener('click', e => {
                    if (!this.acceptingAnswers) return;

                    this.acceptingAnswers = false;
                    const selectedChoice = e.target;
                    const selectedAnswer = selectedChoice.dataset['number'];
                    //check of choice is correct
                    let classToApply = selectedAnswer == this.currentQuestion.answer ?
                        'correct' : 'incorrect'
                    if (classToApply === 'correct') {
                        this.increment(SCORE_POINTS)
                    }
                    //set style for choice correct incorrect
                    selectedChoice.parentElement.classList.add(classToApply);
                    //usetrim timer
                    timerId = setTimeout(() => {
                        selectedChoice.parentElement.classList.remove(classToApply);
                        this.newquest();
                        timerId = null;
                    }, 1000)
                })
            })
        }

        increment(num) {
            this.score += num;
            this.scoreText.innerText = this.score;
        }
    }

    const start = new StartGame();
}