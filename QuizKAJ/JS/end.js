import {navigateTo} from "./index.js";

export function initEnd() {
    const username = document.querySelector('#username')
    const saveScoreBtn = document.querySelector('#saveScoreBtn')
    const finalScore = document.querySelector('#finalScore')
    const mostRecentScore = localStorage.getItem('mostRecentScore')

    const highScores = JSON.parse(localStorage.getItem('highScores'))
        || [];


    finalScore.innerText = mostRecentScore;
    
    //pokud hra skoncila tak reset mostrecent score i po zobrazeni end stranky.
    localStorage.setItem('mostRecentScore', '0');

    username.addEventListener('keyup', () => {
        saveScoreBtn.disabled = !username.value;
    })

    saveScoreBtn.addEventListener('click', e => {
        e.preventDefault()

        const score = {
            score: mostRecentScore,
            name: username.value
        }

        highScores.push(score);
        //sort high score table max -> min
        highScores.sort((a, b) => {
            return b.score - a.score;
        })
        //delete all records from index 5 to continue
        highScores.splice(5);

        localStorage.setItem('highScores', JSON.stringify(highScores));
        navigateTo("#start")
    });
}
